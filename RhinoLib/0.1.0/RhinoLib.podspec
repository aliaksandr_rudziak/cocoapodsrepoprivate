#
# Be sure to run `pod lib lint RhinoLib.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = "RhinoLib"
    s.version          = "0.1.0"
    s.summary          = "Description that describe this obj-c library."
    s.description      = <<-DESC
                        I don't worry about description text

                        * I'm serious
                        * I'm not kidding
                            DESC
    s.homepage         = "https://www.google.com"

    s.license          = 'MIT'
    s.author           = { "Alex Rudyak" => "aliaksandr.rudziak@instinctools.ru" }
    s.source           = {
                            :git => "https://aliaksanrd_rudziak@bitbucket.org/aliaksanrd_rudziak/rhinolibrepo.git",
                            :tag => s.version.to_s
                        }

    s.platform     = :ios, '7.0'
    s.requires_arc = true

    s.source_files = 'Pod/Classes'
    s.resource_bundles = {
        'RhinoLib' => ['Pod/Assets/*.png']
    }

    s.frameworks = 'UIKit'
end
